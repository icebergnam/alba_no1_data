국내 중공업분야의 중규모 기업의 부도 여부 데이터입니다. 입력변수는 재무비율 변수 20개이고, 목표변수는 부도 (1=부도, 0=비부도) 입니다.

File “bankrupt_training_M”: 부도=800개, 비부도=800개

File “bankrupt_testing_M”: 부도=200개, 비부도=200개


1. Training data 모든 X변수, 모든 리코드를 써서, testing data에 대한 예측하기.

2. Training data에서 Outlier detection하여 리코드를 delete 시킨 후, testing data에 대한 예측하기.

3. Training data에서 Outlier detection하고 리코드를 impute 시킨 후 (e.g., by either median or knn method), testing data에 대한 예측하기.

4. 위 3번 방법으로 데이터를 정제한 상태에서, 추가적으로, 입력변수 갯수를 줄여 (e.g., by either decision tree or logistic regression with stepwise selection), testing data에 대한 예측하기.